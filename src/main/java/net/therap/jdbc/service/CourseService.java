package net.therap.jdbc.service;

import net.therap.jdbc.dao.CourseDao;
import net.therap.jdbc.model.Course;
import net.therap.jdbc.model.Error;
import net.therap.jdbc.utils.InputUtils;
import net.therap.jdbc.validator.CourseValidator;
import net.therap.jdbc.view.ErrorView;
import net.therap.jdbc.view.ListView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author sakib.khan
 * @since 12/28/21
 */
public class CourseService {

    private final CourseValidator courseValidator;

    private final CourseDao courseDao;

    public CourseService() {
        courseValidator = new CourseValidator();
        courseDao = new CourseDao();
    }

    public void addCourse() throws SQLException {
        List<Error> errors = new ArrayList<>();
        boolean isInserted = false;
        while (!isInserted) {
            Course course = getNewCourse();
            courseValidator.isValid(course, errors);
            courseValidator.checkIfExists(course, errors);
            if (errors.isEmpty()) {
                courseDao.add(course);
                isInserted = true;
            }
            ErrorView.show(errors);
            errors.clear();
        }
    }

    public void deleteCourse() throws SQLException {
        List<Error> errors = new ArrayList<>();
        System.out.println("Enter Course Code");
        int courseId = InputUtils.takeInteger();
        courseValidator.matchAny(courseId, errors);
        if (errors.isEmpty()) {
            courseDao.delete(courseId);
        }
        ErrorView.show(errors);
    }

    public void showCourseList() throws SQLException {
        List<Course> courseList = courseDao.getAll();
        ListView.showCourses(getSortedList(courseList));
    }

    private Course getNewCourse() {
        System.out.println("Enter Course Name");
        String courseName = InputUtils.takeNextLine();
        System.out.println("Enter Course Title");
        String courseTitle = InputUtils.takeNextLine();
        return new Course(courseName, courseTitle);
    }

    private List<Course> getSortedList(List<Course> courseList) {
        return courseList.stream()
                .sorted((Comparator.comparing(Course::getName)).thenComparing(Course::getTitle))
                .collect(Collectors.toList());
    }
}