package net.therap.jdbc.service;

import net.therap.jdbc.dao.TraineeDao;
import net.therap.jdbc.model.Error;
import net.therap.jdbc.model.Trainee;
import net.therap.jdbc.utils.InputUtils;
import net.therap.jdbc.validator.TraineeValidator;
import net.therap.jdbc.view.ErrorView;
import net.therap.jdbc.view.ListView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author sakib.khan
 * @since 12/28/21
 */
public class TraineeService {

    private final TraineeValidator traineeValidator;

    private final TraineeDao traineeDao;

    public TraineeService() {
        traineeValidator = new TraineeValidator();
        traineeDao = new TraineeDao();
    }

    public void addTrainee() throws SQLException {
        List<Error> errors = new ArrayList<>();
        boolean isInserted = false;
        while (!isInserted) {
            Trainee trainee = getNewTrainee();
            traineeValidator.isValid(trainee, errors);
            traineeValidator.checkIfExists(trainee, errors);
            if (errors.isEmpty()) {
                traineeDao.add(trainee);
                isInserted = true;
            }
            ErrorView.show(errors);
            errors.clear();
        }
    }

    public void deleteTrainee() throws SQLException {
        List<Error> errors = new ArrayList<>();
        System.out.println("Enter Trainee Id");
        int traineeId = InputUtils.takeInteger();
        traineeValidator.anyMatch(traineeId, errors);
        if (errors.isEmpty()) {
            traineeDao.delete(traineeId);
        }
        ErrorView.show(errors);
    }

    public void showTraineeList() throws SQLException {
        List<Trainee> traineeList = traineeDao.getAll();
        ListView.showTrainee(getSortedList(traineeList));
    }

    private Trainee getNewTrainee() {
        System.out.println("Enter First Name");
        String firstName = InputUtils.takeNextLine();
        System.out.println("Enter Last Name");
        String lastName = InputUtils.takeNextLine();
        return new Trainee(firstName, lastName);
    }

    private List<Trainee> getSortedList(List<Trainee> traineeList) {
        return traineeList.stream()
                .sorted(Comparator.comparing(Trainee::getFirstName).thenComparing(Trainee::getLastName))
                .collect(Collectors.toList());
    }
}