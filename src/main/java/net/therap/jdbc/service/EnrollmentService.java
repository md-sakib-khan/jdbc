package net.therap.jdbc.service;

import net.therap.jdbc.dao.CourseDao;
import net.therap.jdbc.dao.EnrollmentDao;
import net.therap.jdbc.dao.TraineeDao;
import net.therap.jdbc.model.Course;
import net.therap.jdbc.model.Enrollment;
import net.therap.jdbc.model.Error;
import net.therap.jdbc.model.Trainee;
import net.therap.jdbc.utils.InputUtils;
import net.therap.jdbc.validator.CourseValidator;
import net.therap.jdbc.validator.EnrollmentValidator;
import net.therap.jdbc.validator.TraineeValidator;
import net.therap.jdbc.view.ListView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * @author sakib.khan
 * @since 12/28/21
 */
public class EnrollmentService {

    private final EnrollmentValidator enrollmentValidator;
    private final CourseValidator courseValidator;
    private final TraineeValidator traineeValidator;

    private final CourseDao courseDao;
    private final TraineeDao traineeDao;
    private final EnrollmentDao enrollmentDao;

    public EnrollmentService() {
        enrollmentValidator = new EnrollmentValidator();
        courseDao = new CourseDao();
        traineeDao = new TraineeDao();
        enrollmentDao = new EnrollmentDao();
        courseValidator = new CourseValidator();
        traineeValidator = new TraineeValidator();
    }

    public void enrollTraineeForCourse() throws SQLException {
        boolean isEnrolled = false;
        while (!isEnrolled) {
            System.out.println("Enter Course Id");
            int courseId = InputUtils.takeInteger();
            System.out.println("Enter Trainee Id");
            int traineeId = InputUtils.takeInteger();

            Course course = courseDao.findById(courseId);
            Trainee trainee = traineeDao.findById(traineeId);
            if (isNull(course) || isNull(trainee)) {
                continue;
            }
            Enrollment enrollment = new Enrollment(course, trainee);
            if (!enrollmentValidator.matchAny(enrollment)) {
                enrollmentDao.enrollCourseForTrainee(enrollment);
                isEnrolled = true;
            } else {
                System.out.println("Enrollment Already Exists");
            }
        }
    }

    public void unEnrollCourseForTrainee() throws SQLException {

        System.out.println("Enter Trainee Id");
        int traineeId = InputUtils.takeInteger();
        System.out.println("Enter Course Id");
        int courseId = InputUtils.takeInteger();

        Course course = courseDao.findById(courseId);
        Trainee trainee = traineeDao.findById(traineeId);
        if (isNull(course) || isNull(trainee)) {
            System.out.println("Course/Trainee Does not Exist !!! Please Try Again");
            return;
        }
        Enrollment enrollment = new Enrollment(course, trainee);
        if (enrollmentValidator.matchAny(enrollment)) {
            enrollmentDao.unEnrollCourseForTrainee(enrollment);
        } else {
            System.out.println("No Match Found");
        }
    }

    public void showEnrollmentsGroupByCourse() throws SQLException {
        List<Enrollment> enrollmentList = enrollmentDao.getEnrollments();
        ListView.showEnrollments(sortByCourse(enrollmentList));
    }

    public void showEnrollmentsGroupByTrainee() throws SQLException {
        List<Enrollment> enrollmentList = enrollmentDao.getEnrollments();
        ListView.showEnrollments(sortByTrainee(enrollmentList));
    }

    private List<Enrollment> sortByCourse(List<Enrollment> enrollmentList) {
        return enrollmentList.stream()
                .sorted(Comparator.comparing((obj) -> obj.getCourse().getName()))
                .collect(Collectors.toList());
    }

    private List<Enrollment> sortByTrainee(List<Enrollment> enrollmentList) {
        return enrollmentList.stream()
                .sorted(Comparator.comparing((obj) -> obj.getTrainee().getFirstName()))
                .collect(Collectors.toList());
    }
}