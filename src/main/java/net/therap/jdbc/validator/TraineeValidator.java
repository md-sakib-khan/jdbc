package net.therap.jdbc.validator;

import net.therap.jdbc.dao.TraineeDao;
import net.therap.jdbc.model.Course;
import net.therap.jdbc.model.Error;
import net.therap.jdbc.model.Trainee;

import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author sakib.khan
 * @since 12/28/21
 */
public class TraineeValidator {

    private static final int NAME_LOWER_LIMIT = 1;
    private static final int NAME_UPPER_LIMIT = 100;
    private static final String NAME_REGEX = "[A-Za-z.\\s]{" + NAME_LOWER_LIMIT + "," + NAME_UPPER_LIMIT + "}";
    private static final String NAME_ERROR_MESSAGE = "Name Should be " + NAME_LOWER_LIMIT + " - " + NAME_UPPER_LIMIT + " Characters";

    private final TraineeDao traineeDao;

    public TraineeValidator() {
        traineeDao = new TraineeDao();
    }

    public void isValid(Trainee trainee, List<Error> errors) {
        boolean isValidFirstName = Pattern.matches(NAME_REGEX, trainee.getFirstName());
        boolean isValidLastName = Pattern.matches(NAME_REGEX, trainee.getLastName());
        if (!isValidFirstName) {
            errors.add(new Error("First " + NAME_ERROR_MESSAGE));
        }
        if (!isValidLastName) {
            errors.add(new Error("Last " + NAME_ERROR_MESSAGE));
        }
    }

    public void checkIfExists(Trainee trainee, List<Error> errors) throws SQLException {
        boolean traineeExists = traineeDao.getAll().stream()
                .anyMatch(item -> item.getFirstName().equalsIgnoreCase(trainee.getFirstName())
                        && item.getLastName().equalsIgnoreCase(trainee.getLastName()));
        if (traineeExists) {
            errors.add(new Error("Trainee Already Exists"));
        }
    }

    public void anyMatch(int traineeId, List<Error> errors) throws SQLException {
        boolean traineeExists = traineeDao.getAll().stream().anyMatch(item -> item.getId() == traineeId);
        if (!traineeExists) {
            errors.add(new Error("Trainee Does Not Exist"));
        }
    }
}