package net.therap.jdbc.validator;

import net.therap.jdbc.dao.EnrollmentDao;
import net.therap.jdbc.model.Course;
import net.therap.jdbc.model.Enrollment;
import net.therap.jdbc.model.Error;
import net.therap.jdbc.model.Trainee;

import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

/**
 * @author sakib.khan
 * @since 12/28/21
 */
public class EnrollmentValidator {

    private final EnrollmentDao enrollmentDao;

    public EnrollmentValidator() {
        enrollmentDao = new EnrollmentDao();
    }

    public boolean matchAny(Enrollment enrollment) throws SQLException {
        List<Enrollment> enrollmentList = enrollmentDao.getEnrollments();
        return enrollmentList.stream()
                .anyMatch(item -> item.getCourse().getId() == enrollment.getCourse().getId()
                        && item.getTrainee().getId() == enrollment.getTrainee().getId());
    }
}