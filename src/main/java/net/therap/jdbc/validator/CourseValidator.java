package net.therap.jdbc.validator;

import net.therap.jdbc.dao.CourseDao;
import net.therap.jdbc.model.Course;
import net.therap.jdbc.model.Error;

import java.sql.SQLException;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author sakib.khan
 * @since 12/28/21
 */
public class CourseValidator {

    private static final int NAME_CHAR_LIMIT = 3;
    private static final int NAME_DIGIT_LIMIT = 3;
    private static final int TITLE_LOWER_LIMIT = 5;
    private static final int TITLE_UPPER_LIMIT = 100;
    private static final String NAME_REGEX = "[A-Za-z]{" + NAME_CHAR_LIMIT + "}(\\d){" + NAME_DIGIT_LIMIT + "}";
    private static final String TITLE_REGEX = "[A-Za-z][A-Za-z\\s]{" + TITLE_LOWER_LIMIT + "," + TITLE_UPPER_LIMIT + "}";
    private static final String NAME_ERROR_MESSAGE = "Course Name Should Start With " + NAME_CHAR_LIMIT + " Characters " + "followed by " + NAME_DIGIT_LIMIT + " Digits";
    private static final String TITLE_ERROR_MESSAGE = "Title Should be " + TITLE_LOWER_LIMIT + " - " + TITLE_UPPER_LIMIT + " Characters";

    private final CourseDao courseDao;

    public CourseValidator() {
        courseDao = new CourseDao();
    }

    public void isValid(Course course, List<Error> errors) {
        boolean isValidName = Pattern.matches(NAME_REGEX, course.getName());
        boolean isValidTitle = Pattern.matches(TITLE_REGEX, course.getTitle());
        if (!isValidName) {
            errors.add(new Error(NAME_ERROR_MESSAGE));
        }
        if (!isValidTitle) {
            errors.add(new Error(TITLE_ERROR_MESSAGE));
        }
    }

    public void checkIfExists(Course course, List<Error> errors) throws SQLException {
        boolean courseExists = courseDao.getAll().stream()
                .anyMatch(item -> item.getName().equalsIgnoreCase(course.getName())
                        || item.getTitle().equalsIgnoreCase(course.getTitle()));
        if (courseExists) {
            errors.add(new Error("Course Already Exists"));
        }
    }

    public void matchAny(int courseId, List<Error> errors) throws SQLException {
        boolean anyMatch = courseDao.getAll().stream().anyMatch(item -> item.getId() == courseId);
        if (!anyMatch) {
            errors.add(new Error("Course Does Not Exist"));
        }
    }
}