package net.therap.jdbc.view;

import net.therap.jdbc.model.Course;
import net.therap.jdbc.model.Enrollment;
import net.therap.jdbc.model.Trainee;

import java.util.List;

/**
 * @author sakib.khan
 * @since 1/3/22
 */
public class ListView {

    public static void showCourses(List<Course> courseList) {
        isEmpty(courseList);
        printLine();
        for (Course course : courseList) {
            System.out.println(course.getId() + " : " + course.getName() + " : " + course.getTitle());
        }
        printLine();
    }

    public static void showTrainee(List<Trainee> traineeList) {
        isEmpty(traineeList);
        printLine();
        for (Trainee trainee : traineeList) {
            System.out.println(trainee.getId() + " : " + trainee.getFirstName() + " " + trainee.getLastName());
        }
        printLine();
    }

    public static void showEnrollments(List<Enrollment> enrollmentList) {
        isEmpty(enrollmentList);
        printLine();
        for (Enrollment enrollment : enrollmentList) {
            System.out.println(enrollment.getCourseName() + " : " + enrollment.getTraineeName());
        }
        printLine();
    }

    private static <T> void isEmpty(List<T> list) {
        if (list.isEmpty()) {
            System.out.println("----No Items----");
        }
    }

    private static void printLine() {
        System.out.println("-------------------------------------------------");
    }
}