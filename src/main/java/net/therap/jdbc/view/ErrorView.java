package net.therap.jdbc.view;

import net.therap.jdbc.model.Error;

import java.util.List;

/**
 * @author sakib.khan
 * @since 1/3/22
 */
public class ErrorView {

    public static void show(List<Error> errors) {
        for (Error error : errors) {
            System.out.println(error.getErrorMessage());
        }
    }
}