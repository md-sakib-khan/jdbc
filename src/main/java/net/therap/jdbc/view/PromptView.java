package net.therap.jdbc.view;

import net.therap.jdbc.controller.MainController;

/**
 * @author sakib.khan
 * @since 12/28/21
 */
public class PromptView {

    public static void showMenu() {
        System.out.println("Choose a menu :");
        System.out.println("Press (" + MainController.ADD_COURSE + ") for Course Entry");
        System.out.println("Press (" + MainController.SHOW_COURSE + ") to Show Courses");
        System.out.println("Press (" + MainController.ADD_TRAINEE + ") to Add Trainee");
        System.out.println("Press (" + MainController.SHOW_TRAINEE + ") to Show Trainee");
        System.out.println("Press (" + MainController.DELETE_COURSE + ") for Delete Course");
        System.out.println("Press (" + MainController.DELETE_TRAINEE + ") to Delete Trainee");
        System.out.println("Press (" + MainController.ENROLL_TRAINEE_FOR_COURSE + ") to Enroll Course for a trainee");
        System.out.println("Press (" + MainController.UNENROLL_COURSE_FOR_TRAINEE + ") to UnEnroll Course for trainee");
        System.out.println("Press (" + MainController.SHOW_ENROLLMENTS_GROUP_BY_COURSE + ") to Show Enrollment Details grouped by Course");
        System.out.println("Press (" + MainController.SHOW_ENROLLMENTS_GROUP_BY_TRAINEE + ") to Show Enrollment Details grouped by Trainee");
        System.out.println("Press (" + MainController.EXIT + ") to Exit");
    }
}