package net.therap.jdbc.dao;

import net.therap.jdbc.model.Course;
import net.therap.jdbc.model.Enrollment;
import net.therap.jdbc.model.Trainee;
import net.therap.jdbc.utils.DBConnectionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sakib.khan
 * @since 12/28/21
 */
public class EnrollmentDao {

    private static final String ENROLL_COURSE_TRAINEE_QUERY = "INSERT INTO enrollments (courseId, traineeId) VALUES(?,?)";
    private static final String UNENROLL_COURSE_FOR_TRAINEE_QUERY = "DELETE FROM enrollments where courseId = ? AND traineeId = ?";
    private static final String SHOW_ENROLLMENT_QUERY =
            "SELECT enrollments.courseId, enrollments.traineeId, courses.name, trainees.firstName, trainees.lastName " +
                    "FROM enrollments " +
                    "INNER JOIN courses ON courses.id = enrollments.courseId " +
                    "INNER JOIN trainees ON trainees.id = enrollments.traineeId";

    private final Connection connection;

    public EnrollmentDao() {
        connection = DBConnectionUtils.getConnection();
    }

    public void enrollCourseForTrainee(Enrollment enrollment) throws SQLException {
        try {
            connection.setAutoCommit(false);
            enrollOrUnenroll(enrollment.getCourse().getId(), enrollment.getTrainee().getId(), ENROLL_COURSE_TRAINEE_QUERY);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            System.out.println("Enroll Operation failed, Error : " + e + " !!! Try Again");
        }
    }

    public void unEnrollCourseForTrainee(Enrollment enrollment) throws SQLException {
        int courseId = enrollment.getCourse().getId();
        int traineeId = enrollment.getTrainee().getId();
        try {
            connection.setAutoCommit(false);
            enrollOrUnenroll(courseId, traineeId, UNENROLL_COURSE_FOR_TRAINEE_QUERY);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            System.out.println("UnEnroll Operation failed, Error : " + e + " !!! Try Again");
        }
    }

    public List<Enrollment> getEnrollments() throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SHOW_ENROLLMENT_QUERY);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Enrollment> enrollmentDetailsList = new ArrayList<>();

        while (resultSet.next()) {
            int courseId = resultSet.getInt("courseId");
            String courseName = resultSet.getString("name");
            int traineeId = resultSet.getInt("traineeId");
            String traineeFirstName = resultSet.getString("firstName");
            String traineeLastName = resultSet.getString("lastName");

            Course course = new Course(courseId, courseName);
            Trainee trainee = new Trainee(traineeId, traineeFirstName, traineeLastName);
            Enrollment enrollment = new Enrollment(course, trainee);
            enrollmentDetailsList.add(enrollment);
        }
        return enrollmentDetailsList;
    }

    private void enrollOrUnenroll(int courseId, int traineeId, String query) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, courseId);
        preparedStatement.setInt(2, traineeId);
        preparedStatement.executeUpdate();
    }
}