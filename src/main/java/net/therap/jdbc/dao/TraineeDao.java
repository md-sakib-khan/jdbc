package net.therap.jdbc.dao;

import net.therap.jdbc.model.Course;
import net.therap.jdbc.model.Trainee;
import net.therap.jdbc.utils.DBConnectionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author sakib.khan
 * @since 12/28/21
 */
public class TraineeDao extends ParentDao {

    private static final String ADD_TRAINEE_QUERY = "INSERT INTO trainees (firstName, lastName) VALUES(?,?)";
    private static final String SHOW_TRAINEE_QUERY = "SELECT * FROM trainees";
    private static final String DELETE_TRAINEE_QUERY = "DELETE FROM trainees WHERE id = ?";
    private static final String DELETE_ENROLLMENT_QUERY = "DELETE FROM enrollments WHERE traineeId = ?";

    private final Connection connection;

    public TraineeDao() {
        connection = DBConnectionUtils.getConnection();
    }

    public <T> void add(T trainee) throws SQLException {
        try {
            connection.setAutoCommit(false);
            Trainee newTrainee = (Trainee) trainee;
            addItem(newTrainee.getFirstName(), newTrainee.getLastName(), ADD_TRAINEE_QUERY);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            System.out.println("Trainee Add failed, Error : " + e + " !!! Try Again");
        }
    }

    public List<Trainee> getAll() throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SHOW_TRAINEE_QUERY);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Trainee> traineeList = new ArrayList<>();
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String firstName = resultSet.getString("firstName");
            String lastName = resultSet.getString("lastName");
            traineeList.add(new Trainee(id, firstName, lastName));
        }
        return traineeList;
    }

    public void delete(int id) throws SQLException {
        try {
            connection.setAutoCommit(false);
            executeDeleteById(id, DELETE_TRAINEE_QUERY);
            executeDeleteById(id, DELETE_ENROLLMENT_QUERY);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            System.out.println("Delete Operation failed, Error : " + e + " !!! Try Again");
        }
    }

    public Trainee findById(int id) throws SQLException {
        try {
            return getAll().stream().filter(item -> item.getId() == id)
                    .collect(Collectors.toList())
                    .stream().findFirst().get();
        } catch (Exception e) {
            return null;
        }
    }
}