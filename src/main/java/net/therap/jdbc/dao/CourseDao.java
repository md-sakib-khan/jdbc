package net.therap.jdbc.dao;

import net.therap.jdbc.model.Course;
import net.therap.jdbc.utils.DBConnectionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author sakib.khan
 * @since 12/28/21
 */
public class CourseDao extends ParentDao {

    private static final String ADD_COURSE_QUERY = "INSERT INTO courses (name, title) VALUES(?,?)";
    private static final String SHOW_COURSE_QUERY = "SELECT * FROM courses";
    private static final String DELETE_COURSE_QUERY = "DELETE FROM courses WHERE id = ?";
    private static final String DELETE_ENROLLMENT_QUERY = "DELETE FROM enrollments WHERE courseId = ?";

    private final Connection connection;

    public CourseDao() {
        connection = DBConnectionUtils.getConnection();
    }

    public <T> void add(T course) throws SQLException {
        try {
            connection.setAutoCommit(false);
            Course newCourse = (Course) course;
            addItem(newCourse.getName(), newCourse.getTitle(), ADD_COURSE_QUERY);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            System.out.println("Add failed, Error : " + e + " !!! Try Again");
        }
    }

    public void delete(int id) throws SQLException {
        try {
            connection.setAutoCommit(false);
            executeDeleteById(id, DELETE_COURSE_QUERY);
            executeDeleteById(id, DELETE_ENROLLMENT_QUERY);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            System.out.println("Delete Operation failed, Error : " + e + " !!! Try Again");
        }
    }

    public List<Course> getAll() throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SHOW_COURSE_QUERY);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Course> courseList = new ArrayList<>();
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String title = resultSet.getString("title");
            courseList.add(new Course(id, name, title));
        }
        return courseList;
    }

    public Course findById(int id) throws SQLException {
        try {
            return getAll().stream().filter(item -> item.getId() == id)
                    .collect(Collectors.toList())
                    .stream().findFirst().get();
        } catch (Exception e) {
            return null;
        }
    }
}