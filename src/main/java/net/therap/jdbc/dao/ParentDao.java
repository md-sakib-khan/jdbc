package net.therap.jdbc.dao;

import net.therap.jdbc.model.Course;
import net.therap.jdbc.model.Trainee;
import net.therap.jdbc.utils.DBConnectionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author sakib.khan
 * @since 1/3/22
 */
public abstract class ParentDao {

    private Connection connection;

    ParentDao() {
        connection = DBConnectionUtils.getConnection();
    }

    public abstract <T> void add(T item) throws SQLException;

    public abstract void delete(int id) throws SQLException;

    public abstract <T> List<T> getAll() throws SQLException;

    public abstract <T> T findById(int id) throws SQLException;

    void addItem(String s1, String s2, String query) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, s1);
        preparedStatement.setString(2, s2);
        preparedStatement.executeUpdate();
    }

    void executeDeleteById(int id, String query) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();
    }
}