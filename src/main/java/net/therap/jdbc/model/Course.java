package net.therap.jdbc.model;

import java.io.Serializable;

/**
 * @author sakib.khan
 * @since 12/20/21
 */
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String name;
    private String title;

    public Course(String name, String title) {
        this.name = name;
        this.title = title;
    }

    public Course(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Course(int id, String name, String title) {
        this.id = id;
        this.name = name;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}