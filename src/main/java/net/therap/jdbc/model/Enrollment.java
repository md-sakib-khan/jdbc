package net.therap.jdbc.model;

import java.io.Serializable;

/**
 * @author sakib.khan
 * @since 12/21/21
 */
public class Enrollment implements Serializable {

    private static final long serialVersionUID = 1L;

    private Course course;
    private Trainee trainee;

    public Enrollment(Course course, Trainee trainee) {
        this.course = course;
        this.trainee = trainee;
    }

    public Course getCourse() {
        return course;
    }

    public Trainee getTrainee() {
        return trainee;
    }

    public String getCourseName() {
        return course.getName();
    }

    public String getTraineeName() {
        return trainee.getFirstName() + " " + trainee.getLastName();
    }
}