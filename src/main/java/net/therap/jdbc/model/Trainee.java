package net.therap.jdbc.model;

import java.io.Serializable;

/**
 * @author sakib.khan
 * @since 12/20/21
 */
public class Trainee implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String firstName;
    private String lastName;

    public Trainee(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Trainee(int traineeId, String firstName, String lastName) {
        this.id = traineeId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}