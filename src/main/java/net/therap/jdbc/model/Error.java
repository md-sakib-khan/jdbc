package net.therap.jdbc.model;

/**
 * @author sakib.khan
 * @since 1/3/22
 */
public class Error {

    private String message;

    public Error(String message) {
        message = message;
    }

    public String getErrorMessage() {
        return message;
    }
}