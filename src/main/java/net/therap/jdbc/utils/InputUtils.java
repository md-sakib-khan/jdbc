package net.therap.jdbc.utils;

import java.util.Scanner;

/**
 * @author sakib.khan
 * @since 12/29/21
 */
public class InputUtils {

    public static Scanner scan = new Scanner(System.in);

    public static int takeInteger() {
        while (true) {
            try {
                int value = scan.nextInt();
                scan.nextLine();
                return value;
            } catch (Exception e) {
                System.out.println("Invalid Input format : Try Again");
                scan.nextLine();
            }
        }
    }

    public static String takeNextLine() {
        return scan.nextLine();
    }
}