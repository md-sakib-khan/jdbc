package net.therap.jdbc.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * @author sakib.khan
 * @since 12/20/21
 */
public class DBConnectionUtils {

    private static Connection connection;

    static {
        try {
            ResourceBundle resourceBundle = ResourceBundle.getBundle("dbconfig");
            String url = resourceBundle.getString("db.url");
            String username = resourceBundle.getString("db.username");
            String password = resourceBundle.getString("db.password");
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        return connection;
    }
}