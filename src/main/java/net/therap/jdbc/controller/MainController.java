package net.therap.jdbc.controller;

import net.therap.jdbc.view.PromptView;
import net.therap.jdbc.utils.InputUtils;

import java.sql.SQLException;

/**
 * @author sakib.khan
 * @since 12/19/21
 */
public class MainController {

    public static final int INITIAL_VALUE = -1;
    public static final int ADD_COURSE = 1;
    public static final int SHOW_COURSE = 2;
    public static final int ADD_TRAINEE = 3;
    public static final int SHOW_TRAINEE = 4;
    public static final int DELETE_COURSE = 5;
    public static final int DELETE_TRAINEE = 6;
    public static final int ENROLL_TRAINEE_FOR_COURSE = 7;
    public static final int UNENROLL_COURSE_FOR_TRAINEE = 8;
    public static final int SHOW_ENROLLMENTS_GROUP_BY_COURSE = 9;
    public static final int SHOW_ENROLLMENTS_GROUP_BY_TRAINEE = 10;
    public static final int EXIT = 0;
    public static final String OUT_OF_RANGE = "----Out Of Range----";

    public static void main(String[] args) throws SQLException {

        CourseController courseController = new CourseController();
        TraineeController traineeController = new TraineeController();
        EnrollmentController enrollmentController = new EnrollmentController();

        int selectedMenu = INITIAL_VALUE;

        while (selectedMenu != EXIT) {

            PromptView.showMenu();
            selectedMenu = InputUtils.takeInteger();

            switch (selectedMenu) {
                case ADD_COURSE: {
                    courseController.addCourse();
                    break;
                }
                case DELETE_COURSE: {
                    courseController.deleteCourse();
                    break;
                }
                case SHOW_COURSE: {
                    courseController.showCourseList();
                    break;
                }
                case ADD_TRAINEE: {
                    traineeController.addTrainee();
                    break;
                }
                case DELETE_TRAINEE: {
                    traineeController.deleteTrainee();
                    break;
                }
                case SHOW_TRAINEE: {
                    traineeController.showTraineeList();
                    break;
                }
                case ENROLL_TRAINEE_FOR_COURSE: {
                    enrollmentController.enrollTraineeForCourse();
                    break;
                }
                case UNENROLL_COURSE_FOR_TRAINEE: {
                    enrollmentController.unEnrollCourseForTrainee();
                    break;
                }
                case SHOW_ENROLLMENTS_GROUP_BY_COURSE: {
                    enrollmentController.showEnrollmentsGroupByCourse();
                    break;
                }
                case SHOW_ENROLLMENTS_GROUP_BY_TRAINEE: {
                    enrollmentController.showEnrollmentsGroupByTrainee();
                    break;
                }
                default: {
                    System.out.println(OUT_OF_RANGE);
                }
            }
        }
    }
}