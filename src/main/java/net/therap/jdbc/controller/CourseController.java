package net.therap.jdbc.controller;

import net.therap.jdbc.service.CourseService;

import java.sql.SQLException;

/**
 * @author sakib.khan
 * @since 12/28/21
 */
public class CourseController {

    private final CourseService courseService;

    CourseController() {
        courseService = new CourseService();
    }

    public void addCourse() throws SQLException {
        courseService.addCourse();
    }

    public void deleteCourse() throws SQLException {
        courseService.deleteCourse();
    }

    public void showCourseList() throws SQLException {
        courseService.showCourseList();
    }
}