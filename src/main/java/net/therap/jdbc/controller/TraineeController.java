package net.therap.jdbc.controller;

import net.therap.jdbc.service.TraineeService;

import java.sql.SQLException;

/**
 * @author sakib.khan
 * @since 12/28/21
 */
public class TraineeController {

    private final TraineeService traineeService;

    TraineeController() {
        traineeService = new TraineeService();
    }

    public void addTrainee() throws SQLException {
        traineeService.addTrainee();
    }

    public void showTraineeList() throws SQLException {
        traineeService.showTraineeList();
    }

    public void deleteTrainee() throws SQLException {
        traineeService.deleteTrainee();
    }
}