package net.therap.jdbc.controller;

import net.therap.jdbc.service.EnrollmentService;

import java.sql.SQLException;

/**
 * @author sakib.khan
 * @since 12/28/21
 */
public class EnrollmentController {

    private final EnrollmentService enrollmentService;

    EnrollmentController() {
        enrollmentService = new EnrollmentService();
    }

    public void enrollTraineeForCourse() throws SQLException {
        enrollmentService.enrollTraineeForCourse();
    }

    public void unEnrollCourseForTrainee() throws SQLException {
        enrollmentService.unEnrollCourseForTrainee();
    }

    public void showEnrollmentsGroupByCourse() throws SQLException {
        enrollmentService.showEnrollmentsGroupByCourse();
    }

    public void showEnrollmentsGroupByTrainee() throws SQLException {
        enrollmentService.showEnrollmentsGroupByTrainee();
    }
}